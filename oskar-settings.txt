[General]
app=oskar_sim_interferometer
version=2.7.6

[sky]
oskar_sky_model/file=model1.txt

[observation]
phase_centre_ra_deg=333.60725
phase_centre_dec_deg=-17.026611
start_frequency_hz=150000000.0
num_channels=5
frequency_inc_hz=10000000.0
start_time_utc=31-01-2020 00:00:00.0
length=12:00:00.0
num_time_steps=2160

[telescope]
input_directory=skalowdummy.tm

[interferometer]
time_average_sec=10.0
ms_filename=/var/scratch/dijkema/skalowdummy.MS
